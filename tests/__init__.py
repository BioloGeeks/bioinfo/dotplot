"""Tests suite for `biologeeks_dotplot`."""

from pathlib import Path

TESTS_DIR = Path(__file__).parent
FIXTURES_DIR = TESTS_DIR / "fixtures"
