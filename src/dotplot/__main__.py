import click


@click.command()
def cli() -> None:
    print("Hello, world")


def main() -> None:
    cli()
