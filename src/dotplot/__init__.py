"""
    biologeeks-dotplot

    A toy implementation of pairwise DNA sequence dotplot
    :author: BioloGeeks <>
    :license: AGPL-3.0
"""

__version__ = "0.1.0-dev"