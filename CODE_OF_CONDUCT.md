Everyone interacting in the biologeeks-dotplot project's codebases and issue trackers is expected to
follow the [PSF Code of Conduct](https://www.python.org/psf/conduct/).
